package com.yixiang.security.common.persistence.dao;

import com.yixiang.security.common.persistence.model.StoreOrder;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单主表 Mapper 接口
 * </p>
 *
 * @author hupeng
 * @since 2019-06-26
 */
public interface StoreOrderMapper extends BaseMapper<StoreOrder> {

}
